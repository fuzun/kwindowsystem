/*
    SPDX-FileCopyrightText: 2014 Martin Gräßlin <mgraesslin@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "kwindoweffects_p.h"
#include "pluginwrapper_p.h"
#include <QWindow>

KWindowEffectsPrivate::KWindowEffectsPrivate()
{
}

KWindowEffectsPrivate::~KWindowEffectsPrivate()
{
}

bool KWindowEffects_isEffectAvailable(const KWindowEffects::Effect effect)
{
    return KWindowEffects::isEffectAvailable(effect);
}

void KWindowEffects_enableBlurBehind(QWindow *const window, const bool enable, const QRegion *const region)
{
    KWindowEffects::enableBlurBehind(window, enable, (region == NULL) ? QRegion{} : *region);
}

void KWindowEffects_enableBackgroundContrast(QWindow *const window,
                                             const bool enable,
                                             qreal contrast,
                                             qreal intensity,
                                             qreal saturation,
                                             const QRegion *const region)
{
    KWindowEffects::enableBackgroundContrast(window, enable, contrast, intensity, saturation, (region == NULL) ? QRegion{} : *region);
}

void KWindowEffects_slideWindow(QWindow *const window, const KWindowEffects::SlideFromLocation location, const int offset)
{
    KWindowEffects::slideWindow(window, (location), offset);
}

namespace KWindowEffects
{
bool isEffectAvailable(Effect effect)
{
    return KWindowSystemPluginWrapper::self().effects()->isEffectAvailable(effect);
}

void enableBlurBehind(QWindow *window, bool enable, const QRegion &region)
{
    KWindowSystemPluginWrapper::self().effects()->enableBlurBehind(window, enable, region);
}

void enableBackgroundContrast(QWindow *window, bool enable, qreal contrast, qreal intensity, qreal saturation, const QRegion &region)
{
    KWindowSystemPluginWrapper::self().effects()->enableBackgroundContrast(window, enable, contrast, intensity, saturation, region);
}

void slideWindow(QWindow *window, SlideFromLocation location, int offset)
{
    KWindowSystemPluginWrapper::self().effects()->slideWindow(window, location, offset);
}
}
